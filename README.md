# O3 Procesado y sensado

**Proyecto de biometria y medioambiente (GTI-3A)**

Proyecto de sensado crowdsensing mediante el que con un microcontrolador que toma medidas de la calidad del aire cada cierto tiempo, se envian a traves de beacons bluetooth BLE; estos se reciben en el movil del usuario (Android), se procesan, se georeferencian y se envian a un servidor REST.

Con estos datos, se genera un mapa de contaminacion de las areas en las que se han tomado muestras, y es accesible para todos los usuarios de forma publica a traves de una aplicacion web en [este enlace](
https://adgaran1.upv.edu.es/app/).


**Diseño de la aplicacion actual completo [AQUI](https://gitlab.com/gti3_team05/o3-sensing-map/-/blob/develop/Documentacion/Sprint%200/Dise%C3%B1o%20completo.pdf)**

