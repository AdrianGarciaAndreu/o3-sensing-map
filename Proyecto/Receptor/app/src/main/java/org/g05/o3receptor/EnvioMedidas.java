package org.g05.o3receptor;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Clase asincrona para enviar una medida a un servidor mediante una API de tipo REST
 * @author Adrián García Andreu
 * @version 1.1
 */
public class EnvioMedidas extends AsyncTask<String, Void, String> {

    private Activity actividadQueEjecuta; //Actividad que ejectua el envio de la medida
    private ProgressDialog dialogoDeEspera;

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Constructor de la clase
     * --------------------------------------------------------------
     * actividadQueEjecuta:Activity --> EnvioMedidas()
     * --------------------------------------------------------------
     * @param actividadQueEjecuta actividad desde la que se crea
     *                            el envio de la medidas, esta es
     *                            usada para poder acceder al cotnexto
     *                            de la actividad y tener control
     *                            sobre lo que se muestra
     *                            en pantalla en funcion de
     *                            la tarea en 2o plano
     */
    public EnvioMedidas(Activity actividadQueEjecuta){
        this.actividadQueEjecuta = actividadQueEjecuta;
        this.dialogoDeEspera = new ProgressDialog(actividadQueEjecuta);
    }



    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Muestra un dialogo en pantalla para impedir la interaccion del usuario
     * mientras se están realizando peticiones al servidor
     * --------------------------------------------------------------
     * onPreExecute()
     * --------------------------------------------------------------
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        // Muestra el dialogo para evitar que el usuario interaccione con la app mientras se envia la peticion
        this.dialogoDeEspera.setTitle("Peticion REST");
        this.dialogoDeEspera.setMessage("Enviando beacon al servidor REST...");
        this.dialogoDeEspera.setCancelable(false);
        if(!this.dialogoDeEspera.isShowing()){
            this.dialogoDeEspera.show();
        }


    }



    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * datos:Texto --> doInBackground()
     * --------------------------------------------------------------
     * @param datos contiene en la posicion 0 la URL del servidor REST
     *              y en la posicion 1 los parametros y valores
     *              a procesar por la peticion
     */
    @Override
    protected String doInBackground(String... datos) {

        String retorno = null;
        Log.d("RESP","INICIO");


        URL url;
        String parametros;
        HttpURLConnection connection = null;

        try {
            url = new URL(datos[0]);
            parametros = datos[1];

            // Crea una conexion HTTP para la url determinada
            connection = (HttpURLConnection) url.openConnection();

            // Se establece el tipo de la conexion
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Accept", "*/*");

            // Escribte los datos a enviar en la peticion REST de tipo POST
            connection.setDoOutput(true);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
            writer.write(parametros);
            writer.close();


            // Captura la respuesta
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String linea="", resultado = "";
            while ((linea = reader.readLine()) !=null){
                resultado += linea;
            }
            //Devuelve el resultado
            if(resultado.length()>0){
                retorno = resultado;
            }


            // Establece la conexion
            connection.connect();

            //Muestra por consola la respuesta
            Log.e("Response", connection.getResponseMessage() + "");

        } catch (Exception e) {
            Log.e(e.toString(), "Fallo en la solicitud");
        } finally {
            // Se asegura el cierre de conexion, en caso de haberla establecido
            if(connection!=null){
                connection.disconnect();
            }
        }


    return retorno;
    }



    /** --------------------------------------------------------------
     * --------------------------------------------------------------
     * Cierra el dialogo que se muestra en pantalla
     * para devolver la visibilidad y el control de
     * interaccion al usuario
     * --------------------------------------------------------------
     * onPostExecute()
     * --------------------------------------------------------------
     */
    @Override
    protected void onPostExecute(String resultado) {

        super.onPostExecute(resultado);

        //---------------------------------------------------------------
        //---------------------------------------------------------------
        // Se procesa el resultado

        boolean todoCorrecto = false;
        String mensajeUsuario = "Beacon no enviado, fallo en el servidor! "; // Mensaje por defecto, a mostrar al usuario, si el servidor no devuelve un verdadero, algo ha ido mal

        // Si el servidor devuelve un verdadero, se entiende que las operaciones han ido como se esperaba
        try {
            JSONObject resultadoJSON = new JSONObject(resultado);
            todoCorrecto = resultadoJSON.getBoolean("datos");

           // Log.d("INFO API", "JSON: "+resultadoJSON.toString());
           // Log.d("INFO API", "JSON: "+todoCorrecto);
            if(todoCorrecto){
                mensajeUsuario = "Beacon enviado al servidor!";
            }

        } catch (JSONException ex){
            Log.e("ERROR API", "No se ha podido procesar el resultado como JSON --> resultado: "+resultado);
        } // Fin del procesado de la respuesta del servidor



        // Cierra el dialogo
        if(this.dialogoDeEspera.isShowing()){
            this.dialogoDeEspera.dismiss();
        }


        // Se muestra un mensaje emergente indicando que el beacon se ha enviado al servidor
        final Snackbar snackbar = Snackbar.make(actividadQueEjecuta.findViewById(R.id.linearLayout),
                mensajeUsuario, 5000);

        snackbar.setAction("Cerrar", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
        // fin del snackbar


        // -------------------------------------------------
        // Vuelve a la busqueda
        MainActivity ma = (MainActivity)this.actividadQueEjecuta;
        ma.iniciarEscaneo();



    }
}


