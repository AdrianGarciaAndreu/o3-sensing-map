package org.g05.o3receptor;


import android.location.Location;

/**
 * POJO que contiene una medida capturada de un beacon BLE, de un tipo determinado
 * en unas coordenadas geograficas
 * @author Adrián García Andreu
 * @version 1.1
 */
public class Medida {


    private String tipo; // tipo de la medida
    private int medida; // valor de la medida del sensor
    private Location ubicacion; // Ubicacion donde se tomo la medida



    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Constructor de clase
     * --------------------------------------------------------------
     * @param tipo Tipo de la medida
     * @param medida Valor de la medida
     * @param ubicacion Ubicacion de la medida
     */
    public Medida(String tipo, int medida, Location ubicacion){
        this.tipo = tipo;
        this.medida = medida;
        this.ubicacion = ubicacion;
    } // ()





    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * getTipo() --> Texto
     * --------------------------------------------------------------
     * @return Texto con el tipo de la medida
     */
    public String getTipo() {
        return tipo;
    } // ()



    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * getMedida() --> Z
     * --------------------------------------------------------------
     * @return Valor de la medida tomada
     */
    public int getMedida() {
        return medida;
    } // ()



    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * getUbicacion() --> Location
     * --------------------------------------------------------------
     * @return Ubicacion en la que se tomo la medida
     */
    public Location getUbicacion() {
        return ubicacion;
    } // ()





}
