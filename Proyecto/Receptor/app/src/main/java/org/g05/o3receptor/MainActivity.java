package org.g05.o3receptor;
// ------------------------------------------------------------------
// ------------------------------------------------------------------

// ------------------------------------------------------------------
// ------------------------------------------------------------------
import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

// ------------------------------------------------------------------
// ------------------------------------------------------------------

/**
 * Actividad principal para la interaccion del usuario, la cual busca beacons BLE
 * del UUID especificado en bucle, y en caso de encontrarlos y que estos sean validos
 * se envian a un servidor REST
 *
 * @author Adrián García Andreu
 * @version 1.1
 */
public class MainActivity extends AppCompatActivity {

    private final String DISPOSITIVO_UUID = "EPSG-PROY-3A-G05"; //UUID A BUSCAR PARA LOS BEACONS
    private final int TIEMPO_MAX_ENTRE_BEACONS = 7000; // TIEMPO MAXIMO SIN LEER BEACONS, EN SEGUNDOS

    private final int SOLICITUD_PERMISO_LOCALIZACION = 99;
    private final int SOLICITUD_ENCENDIDO_BL = 98;


    private long momentoUltimoBeacon; // Momento en el que se detecto el ultimo beacon
    private ProcesadorMediciones pm;

    // Manejador y Runnable para ejecutar en segundo plano la comprobacion de tiempo desde el ultimo beacon recibido
    private final Handler manejadorBLEBeaconsTiempo = new Handler();
    private Runnable ejecutableBLEBeaconsTiempo;



    // --------------------------------------------------------------
    // --------------------------------------------------------------
    private static String ETIQUETA_LOG = ">>>>";

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    private BluetoothAdapter.LeScanCallback  callbackLeScan = null;

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    private void buscarTodosLosDispositivosBTLE() {
        this.callbackLeScan = new BluetoothAdapter.LeScanCallback() {
            @Override
            public void onLeScan(BluetoothDevice bluetoothDevice, int rssi, byte[] bytes) {

                //
                //  se ha encontrado un dispositivo
                //
                mostrarInformacionDispositivoBTLE( bluetoothDevice, rssi, bytes );

            } // onLeScan()
        }; // new LeScanCallback

        //
        //
        //


        boolean resultado = BluetoothAdapter.getDefaultAdapter().startLeScan( this.callbackLeScan );

        // Log.d(ETIQUETA_LOG, " buscarTodosLosDispositivosBTL(): startLeScan(), resultado= " + resultado );
    } // ()

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    private void mostrarInformacionDispositivoBTLE( BluetoothDevice bluetoothDevice, int rssi, byte[] bytes) {

        Log.d(ETIQUETA_LOG, " ****************************************************");
        Log.d(ETIQUETA_LOG, " ****** DISPOSITIVO DETECTADO BTLE ****************** ");
        Log.d(ETIQUETA_LOG, " ****************************************************");
        Log.d(ETIQUETA_LOG, " nombre = " + bluetoothDevice.getName());
        Log.d(ETIQUETA_LOG, " dirección = " + bluetoothDevice.getAddress());
        Log.d(ETIQUETA_LOG, " rssi = " + rssi );

        Log.d(ETIQUETA_LOG, " bytes = " + new String(bytes));
        Log.d(ETIQUETA_LOG, " bytes (" + bytes.length + ") = " + Utilidades.bytesToHexString(bytes));

        TramaIBeacon tib = new TramaIBeacon(bytes);

        Log.d(ETIQUETA_LOG, " ----------------------------------------------------");
        Log.d(ETIQUETA_LOG, " prefijo  = " + Utilidades.bytesToHexString(tib.getPrefijo()));
        Log.d(ETIQUETA_LOG, "          advFlags = " + Utilidades.bytesToHexString(tib.getAdvFlags()));
        Log.d(ETIQUETA_LOG, "          advHeader = " + Utilidades.bytesToHexString(tib.getAdvHeader()));
        Log.d(ETIQUETA_LOG, "          companyID = " + Utilidades.bytesToHexString(tib.getCompanyID()));
        Log.d(ETIQUETA_LOG, "          iBeacon type = " + Integer.toHexString(tib.getiBeaconType()));
        Log.d(ETIQUETA_LOG, "          iBeacon length 0x = " + Integer.toHexString(tib.getiBeaconLength()) + " ( "
                + tib.getiBeaconLength() + " ) ");
        Log.d(ETIQUETA_LOG, " uuid  = " + Utilidades.bytesToHexString(tib.getUUID()));
        Log.d(ETIQUETA_LOG, " uuid  = " + Utilidades.bytesToString(tib.getUUID()));
        Log.d(ETIQUETA_LOG, " major  = " + Utilidades.bytesToHexString(tib.getMajor()) + "( "
                + Utilidades.bytesToInt(tib.getMajor()) + " ) ");
        Log.d(ETIQUETA_LOG, " minor  = " + Utilidades.bytesToHexString(tib.getMinor()) + "( "
                + Utilidades.bytesToInt(tib.getMinor()) + " ) ");
        Log.d(ETIQUETA_LOG, " txPower  = " + Integer.toHexString(tib.getTxPower()) + " ( " + tib.getTxPower() + " )");
        Log.d(ETIQUETA_LOG, " ****************************************************");

    } // ()

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    private void buscarEsteDispositivoBTLE(final UUID dispositivoBuscado ) {

        this.callbackLeScan = new BluetoothAdapter.LeScanCallback() {
            @Override
            public void onLeScan(BluetoothDevice bluetoothDevice, int rssi, byte[] bytes) {

                //
                // dispostivo encontrado
                //

                TramaIBeacon tib = new TramaIBeacon( bytes );
                String uuidString =  Utilidades.bytesToString( tib.getUUID() );

                 if ( uuidString.compareTo( Utilidades.uuidToString( dispositivoBuscado ) ) == 0 )  {

                    mostrarInformacionDispositivoBTLE( bluetoothDevice, rssi, bytes );

                     // -------------------------------------------------
                     // Al detectar un beacon de nuestro sensor...
                     momentoUltimoBeacon = System.currentTimeMillis(); // Actualizo el momento
                     detenerBusquedaDispositivosBTLE(); // Detengo la busqueda

                     haLlegadoUnBeacon(tib);

                } else {
                    Log.d( MainActivity.ETIQUETA_LOG, " * UUID buscado >" +
                            Utilidades.uuidToString( dispositivoBuscado ) + "< no concuerda con este uuid = >" + uuidString + "<");

                    centinelaBLE();
                }

            } // onLeScan()


        }; // new LeScanCallback

        //
        //
         BluetoothAdapter.getDefaultAdapter().startLeScan( this.callbackLeScan );

        // Actualizado los datos de la pantalla
        final TextView tv_estado = (TextView)findViewById(R.id.tv_indicdaorEstado);
        tv_estado.setText("Buscando beacon");
        //


    } // ()

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    private void detenerBusquedaDispositivosBTLE() {
        if ( this.callbackLeScan == null ) {
            return;
        }

        //
        //
        //
        BluetoothAdapter.getDefaultAdapter().stopLeScan(this.callbackLeScan);
        this.callbackLeScan = null;



        // Detiene el segundo hilo
        manejadorBLEBeaconsTiempo.removeCallbacks(this.ejecutableBLEBeaconsTiempo);


        /////////////////////////////////////////////
        //// PRUEBA, TEMPORAL SOLO PARA MOSTRAR INFO Y DEPURAR
        // Actualizo la pantalla
        /////////////////////////////////////////////

            final TextView tv_estado = (TextView)findViewById(R.id.tv_indicdaorEstado);
            tv_estado.setText("En espera");

        /////////////////////////////////////////////
        /////////////////////////////////////////////

    } // ()

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    public void botonBuscarDispositivosBTLEPulsado( View v ) {
        Log.d(ETIQUETA_LOG, " boton buscar dispositivos BTLE Pulsado" );
        this.buscarTodosLosDispositivosBTLE();
    } // ()

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    public void botonBuscarNuestroDispositivoBTLEPulsado( View v ) {
        Log.d(ETIQUETA_LOG, " boton nuestro dispositivo BTLE Pulsado" );

        //this.buscarEsteDispositivoBTLE( Utilidades.stringToUUID( this.DISPOSITIVO_UUID ) );

        //Detiene los procesos de busqueda y los vuelve a iniciar 
        detenerBusquedaDispositivosBTLE();
        iniciarEscaneo();
    } // ()

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    public void botonDetenerBusquedaDispositivosBTLEPulsado( View v ) {
        Log.d(ETIQUETA_LOG, " boton nuestro dispositivo BTLE Pulsado" );
        this.detenerBusquedaDispositivosBTLE();
    } // ()


    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Funcion que recibe un beacon, extrae sus valores y si estos son
     * validos se obtiene la ubicacion y se envian al servidor REST
     * la medida y la posicion en la que ha sido tomada
     * --------------------------------------------------------------
     * TramaIBeacon --> haLlegadoUnBeacon()
     * --------------------------------------------------------------
     * @param beacon  Beacon recibido por el dispositivo
     */
    private void haLlegadoUnBeacon(TramaIBeacon beacon) {



        /////////////////////////////////////////////
        //// PRUEBA, TEMPORAL SOLO PARA MOSTRAR INFO Y DEPURAR
        // Actualizo la pantalla
        /////////////////////////////////////////////

            final TextView tv_estado = (TextView)findViewById(R.id.tv_indicdaorEstado);
            tv_estado.setText("Beacon detectado!");

        /////////////////////////////////////////////
        /////////////////////////////////////////////


        int major = Utilidades.bytesToInt(beacon.getMajor());
        int minor = Utilidades.bytesToInt(beacon.getMinor());

        // Se extrae el valor de la medida del sensor correspondiente del major y del minor
        final Integer medicion = this.pm.extraerMedicion(major, minor);

        // Si hay una medida buena, se envia
        if (medicion!=null){

            // Se actualiza la ultima posicion GPS del usuario
            this.pm.actualizarUltimaPosicionGPS();

                // --------------------------------------------------------
                // Tras 3 segundos, se realizan 5 intentos de obtener la ubicacion (1 por segundo),
                // si esta se obtiene, se envia la medida, si no, se descarta
                Handler postProcesador = new Handler();
                postProcesador.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        int intentos = 5; // Nuemero de intentos
                        boolean ubicacionObtenida = false;
                        while(intentos>0){
                            if(pm.isUbicacionActualizada()){ //Si la ubicacion se ha obtenido, se envia la medida
                                ubicacionObtenida = true;
                                Log.d("ENVIO","Se envia");

                                //Se crea la medida
                                Medida m = new Medida(pm.getTiposMedida().get(pm.getUltimoTipoVisto()),
                                        medicion, pm.getUltimaUbicacionVista());

                                //Se envia la medida
                                pm.enviarMedida(m);

                                /////////////////////////////////////////////
                                //// PRUEBA, TEMPORAL SOLO PARA MOSTRAR INFO Y DEPURAR
                                // Actualizo la pantalla
                                /////////////////////////////////////////////
                                    TextView tv_valor = (TextView)findViewById(R.id.tv_valorMedida);
                                    TextView tv_tipo = (TextView)findViewById(R.id.tv_tipoMedida);
                                    TextView tv_lat = (TextView)findViewById(R.id.tv_latitudMedida);
                                    TextView tv_lon = (TextView)findViewById(R.id.tv_longitudMedida);
                                    TextView tv_momento = (TextView)findViewById(R.id.tv_momentoMedida);


                                    tv_valor.setText("Valor "+String.valueOf(m.getMedida())+" ug/m^3");
                                    tv_tipo.setText("Tipo "+m.getTipo());
                                    tv_lat.setText("Latitud \n"+String.valueOf(m.getUbicacion().getLatitude()));
                                    tv_lon.setText("Longitud \n"+String.valueOf(m.getUbicacion().getLongitude()));

                                    tv_estado.setText("Beacon enviado");

                                    String fecha = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
                                    String tiempo = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());

                                    tv_momento.setText("Momento de deteccion\n "+fecha+" "+tiempo);

                                /////////////////////////////////////////////
                                /////////////////////////////////////////////

                                break; // Si la medida se ha enviado, se sale del bucle

                            } else { //Si no se ha obtenido la ubicacion se cancela el envio

                                esperar(1000);
                                intentos--; // un intento menos

                            } // end

                        } // end while()

                        if(!ubicacionObtenida) { //Si la ubicacion no se obtiene, se vuelve a la busqueda
                            buscarEsteDispositivoBTLE( Utilidades.stringToUUID( DISPOSITIVO_UUID ) );
                        }

                    } // end run()
                }, 3000); // Se espera 3 segundos por defecto para la obtencion de la localizacion.

        } else{ // Si la medida no es valida

            // Se vuelve a la busqueda de beacons
            iniciarEscaneo();

        } //



    } // ()




    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Espera una cantidad de tiempo en milisegundos
     * --------------------------------------------------------------
     * N --> esperar()
     * --------------------------------------------------------------
     * @param tiempo  Tiempo a esperar en ms
     */
    private void esperar(long tiempo){
        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() { }
        }, tiempo); //()
    }


    // --------------------------------------------------------------
    // --------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //Cargo la barra de acción de la actividad
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getTitle());

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);

        //Se instancia el procesador de mediciones
        this.pm = new ProcesadorMediciones(this);


        // Se comprueba el bluetooth
        comprobarBlueetooth();

        // Se solicita el permiso de localizacion al usuario en caso de no estar concedido
        boolean concedido = false;
        concedido = solicitarPermisos();

        // Si se tiene permiso de localizacion, comienza el escaneo, sino se vuelve a solicitar
        if(concedido){ iniciarEscaneo(); }
        //else{ solicitarPermisos();}

    } // onCreate()




    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Inicia el escaneo de beacons BLE en la aplicacion
     * --------------------------------------------------------------
     * iniciarEscaneo()
     * --------------------------------------------------------------
     */
    public void iniciarEscaneo(){

        // Se actualiza el tiempo en el que se empieza a escanear beacons
        this.momentoUltimoBeacon = System.currentTimeMillis();

        // Se ejectua un 2o hilo, para contabilizar que el tiempo entre beacons
        ejecutableBLEBeaconsTiempo = new Runnable() {
            @Override
            public void run() {

                // Comprueba cuando fue el ultimo beacon valido en ser detectado
                centinelaBLE();
                manejadorBLEBeaconsTiempo.postDelayed(ejecutableBLEBeaconsTiempo, 1000); //Ejecuta cada segundo en bucle
            }
        };
        manejadorBLEBeaconsTiempo.post(ejecutableBLEBeaconsTiempo); // Inicia el 2o hilo


        //Busca beacons
        this.buscarEsteDispositivoBTLE( Utilidades.stringToUUID( this.DISPOSITIVO_UUID ) );
    } // ()




    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     *  Comprueba en el instante actual cuanto tiempo hace que no se recibe
     *  un beacon valido, en caso de no recibir un beacon valido en dicho tiempo,
     *  detiene el buscador de beacons y lo vuelve a inciar, minimizando
     *  la perdida de beacons por casuistica de mala sincronizacion entre
     *  la busqueda y la emision de beacons
     * --------------------------------------------------------------
     * centinelaBLE()
     */
    private void centinelaBLE(){

        long actual = System.currentTimeMillis(); // Se actualiza el tiempo
        //comprobarBlueetooth(); // Se comprueba si el bluetooth sigue encendido

        if((actual -momentoUltimoBeacon )> TIEMPO_MAX_ENTRE_BEACONS){
            Log.d("CENTINLEA", "REINICIO");
            this.momentoUltimoBeacon = System.currentTimeMillis();
            this.detenerBusquedaDispositivosBTLE();
            // Reiniciar
            iniciarEscaneo();
            //this.buscarEsteDispositivoBTLE(Utilidades.stringToUUID(DISPOSITIVO_UUID));
        }

    }



    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     *  Solicita al usuario por pantalla la convesion de los permisos
     *  de ubicacion, en caso de no tenerlos
     * --------------------------------------------------------------
     * solicitarPermisos() --> Booleano
     * --------------------------------------------------------------
     * @return Devuelve si los permisos han sido concedidos o no
     */
    private boolean solicitarPermisos(){

        boolean resultado;

        // Si no se tienen condedidos los permisos de localizacion
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Si se requiere que el usuario acepte o no manualmente o no (depende del sistema)
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                //Crea un dialogo para solicitar el permiso al usuario
                new AlertDialog.Builder(this)
                        .setTitle("Permiso de localizacion")
                        .setMessage("Para captar las medidas con el sensor de O3 y georeferenciarlas " +
                                "se requiere del permiso de localizacion del dispositivo")
                        .setPositiveButton("Conceder", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MainActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        SOLICITUD_PERMISO_LOCALIZACION);
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //No hacer nada, en caso de no obtener el permiso, se vuelve a solciitar,
                                // pues no se puede usar la aplicacion sin este permiso
                                solicitarPermisos();
                            }
                        })
                        .create()
                        .show(); //Muestra el dialogo


            } else {
                // Si no se requiere de la interaccion del usuario, se solicitan los permisos al sistema directamente.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        SOLICITUD_PERMISO_LOCALIZACION);
            } // end if


            resultado = false;
        } else {
            // Si los permisos ya estaban concedidos
            resultado = true;
        }

        return resultado;

    } // ()


    /**
     *
     * @return
     */
    private void comprobarBlueetooth(){

        BluetoothAdapter bla = BluetoothAdapter.getDefaultAdapter();

        if(bla!=null){
            if (!bla.isEnabled()) {
                detenerBusquedaDispositivosBTLE(); // Se detiene la busqueda

                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, SOLICITUD_ENCENDIDO_BL);

            }
        } // en caso de que sea nulo, el dispositivo no tiene Bluetooth (no es compatible)

    }


    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     *  Callback para procesar la resolucion de la convesion de permisos
     * --------------------------------------------------------------
     * requestCode:Z, permissions:Texto, grantResults:[Z] --> onRequestPermissionsResult()
     * --------------------------------------------------------------
     * @param requestCode codigo de la solicitud
     * @param permissions vector de permisos
     * @param grantResults vector que contiene la resolucion de los permisos
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            // ----------------------------------------------------------
            // Permiso de localizacion
            case SOLICITUD_PERMISO_LOCALIZACION: {

                // Si hay algun permiso concedido para la app
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // Se comprueba si son los permisos de localizacion
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        // -------------------------------------------
                        // Se inicia el escaneo de beacons
                        iniciarEscaneo();
                    }

                } else {
                    // Si el permiso de deniega, no se hace nada
                    // se requeire del permiso para ejecutar la aplicacion,
                    Log.e("ERROR", "Permiso de localizacion denegado...");
                    solicitarPermisos();

                }
                return;
            } // Fin permiso de localizacion

        } // end switch


    } // ()


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Si es el encendido del bluetooth
        if(requestCode == SOLICITUD_ENCENDIDO_BL){
            if(resultCode == RESULT_OK){ //Bluetooth encendido
                iniciarEscaneo();
            } else{
                    // Bluetooth NO encendido
                    comprobarBlueetooth(); // Se vuelve a solicitar el encendido del Blueetooth
            }
        }

    }
} // class
// --------------------------------------------------------------
// --------------------------------------------------------------
// --------------------------------------------------------------
// --------------------------------------------------------------

