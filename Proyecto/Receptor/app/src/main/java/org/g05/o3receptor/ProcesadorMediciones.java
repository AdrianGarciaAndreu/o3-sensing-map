package org.g05.o3receptor;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Looper;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Procesa las medicionas a partir de un beacon de tipo iBeacon y las envía
 * @author Adrián García Andreu
 * @version 1.1
 */
public class ProcesadorMediciones {


    // --------------------------------------------------------------
    // Direccion del servidor con la API REST
    private final String RECURSO_API_MEDIDAS = "https://adgaran1.upv.edu.es/api/v1.0/medidas/"; //API servidor público

    //API servidor local para testear
    // private final String RECURSO_API_MEDIDAS = "http://192.168.0.14/proyecto3A/api/v1.0/medidas/";


    private Activity actividadQueEjecuta; // Actividad de Android que ejecuta al procesador de mediciones
    private int ultimoContadorVisto;     // Contador ultima medida tomada (para evitar beacons repetidos)
    private HashMap<Integer, String> tipoMedida; // Tipos de medida posibles
    private int ultimoTipoVisto; // Ultimo tipo de medida visto (clave del par clave/valor tipoMedida)


    // --------------------------------------------------------------
    // Parametros para obtener la ubicacion
    private FusedLocationProviderClient manejadorUbicaciones; // Servicio de localizacion del sistema Android
    private LocationRequest solicitudUbicacion; // Peticion de ubicacion
    private LocationCallback callbackUbicacion; // Callback para la peticion de ubicacion
    private Location ultimaUbicacionVista; // Ultima actualizacion conocida del usuario
    private boolean ubicacionActualizada; // Determina si la ubicacion se ha actualizado al tomar la ultima medida, o es anterior.


     /**
      * --------------------------------------------------------------
      * --------------------------------------------------------------
      * Constructor de clase, se inicializan las variables que lo requieren
      * y a este se le pasa como parametro la actividad desde la que se ejectua
      * para poder acceder a esta y a su contexto
      * -------------------------------------------------------------
      * Activity --> ProcesadorMediciones()
      * -------------------------------------------------------------
      * @param actividadQueEjecuta Actividad desde la que se ejectua la clase, se requeire para tener acceso a la ubicacion
     */
    public ProcesadorMediciones(Activity actividadQueEjecuta) {

        this.ultimoContadorVisto = 0;
        this.actividadQueEjecuta = actividadQueEjecuta;
        this.ubicacionActualizada = false;

        // Quizas como constante externo a la clase, mejor.
        //Guardo los tipos de medidas posibles
        tipoMedida = new HashMap<>();
        tipoMedida.put(11, "O3");
        tipoMedida.put(12, "TEMPERATURA");
        tipoMedida.put(13, "RUIDO");


        //Se obtiene el servicio de ubicacion del dispositivo
        manejadorUbicaciones = LocationServices.getFusedLocationProviderClient(actividadQueEjecuta);


    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Se extraen los valores de un beacon a traves de los bytes
     * de este, y se comprueba si el beacon es valido
     * --------------------------------------------------------------
     *  major:Z, minor:Z --> extraerMediciones() --> ValorDeLaMedida:Z
     * ---------------------------------------------------------------
     * @param major Byte del beacon BLE, contiene el tipo de medida y un contador para la comrpobacion de integridad cada uno en 8 bits de sus 16 totales
     * @param minor Byte del beacon BLE, contiene el valor de la medida tomada
     *
     * @return Si la medida es valida, se retorna el valor del minor, si no lo es, devolvera un nulo
    */
    public Integer extraerMedicion(int major, int minor) {

        EnteroEn2Bytes valoresMajor = descomponerEn2bytes(major);

        int medidaTipo = valoresMajor.getMSB(); // 1 byte del major, ID de la medida
        int contadorBeacon = valoresMajor.getLSB(); // 2o byte del major, contador del beacon


        //Si el ultimo beacon recibido no es el mismo que se habia recibido, se actualiza el contador y se devuelve la medida
        if (contadorBeacon != this.ultimoContadorVisto) {

            Log.e("ERROR", "CONTADOR BE " + contadorBeacon);

            this.ultimoContadorVisto = contadorBeacon;
            this.ultimoTipoVisto = medidaTipo;
            return minor;

        } else {
            return null;
        } //No se retorna nada


    } // ()


    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Se actualiza la ultima posicion GPS conocida del usuario
     * --------------------------------------------------------------
     *  actualizarUltimaPosicionGPS()
     */
    public void actualizarUltimaPosicionGPS() {

        //Se crea una solicitud de ubicacion
        solicitudUbicacion = LocationRequest.create();
        solicitudUbicacion.setInterval(10000);
        solicitudUbicacion.setFastestInterval(5000);
        solicitudUbicacion.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(solicitudUbicacion);

        // ---------------------------------------------------------
        // Callback para la obtencion de la ubicacion
        callbackUbicacion = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                //Si se obtiene una ubicacion...
                for (Location location : locationResult.getLocations()) {

                    // Se muestra por consola
                    Log.d("Location", "lat: " + location.getLatitude());
                    Log.d("Location", "lon: " + location.getLongitude());

                    ultimaUbicacionVista = location; //Actualizo la ubicacion
                    ubicacionActualizada = true; // Marco la ubicacion como actualizada (se usa como un flag)

                    manejadorUbicaciones.removeLocationUpdates(callbackUbicacion); // Se para la obtencion de localizacion

                }
            }
        };

        // ---------------------------------------------------------
        // Se crea una tarea para solicitar la ubicacion de forma asincrona
        // y esperar su retorno en el callback
        SettingsClient client = LocationServices.getSettingsClient(actividadQueEjecuta);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build()); // Se validan las opciones ubicacion, antes de iniciar el servicio

        //Cuando la tarea es completada...
        task.addOnSuccessListener(actividadQueEjecuta, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {

                // ---------------------------------------------------------
                // Se comprueba que se tenga el permiso de localizacion...
                if (ActivityCompat.checkSelfPermission(actividadQueEjecuta, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(actividadQueEjecuta, Manifest.permission.ACCESS_COARSE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED) {

                    // No se obtiene la ubicacion, y el beacon no se envia
                    return;
                }

                // Se inicia la solicitud de ubicacion
                manejadorUbicaciones.requestLocationUpdates(solicitudUbicacion, callbackUbicacion, Looper.getMainLooper());
           }
       }); // Fin de la tarea



    } // ()






    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Se envia una medida valida al servidor REST
     * --------------------------------------------------------------
     *  Medida --> enviarMedida()
     * --------------------------------------------------------------
     * @param m Medida obtenida del beacon BLE con tipo y coordenadas GPS asociadas
     */
    public void enviarMedida(Medida m){
        this.ubicacionActualizada = false; // Marco como falso la ubicacion actualizada, para la proxima medida que se tome


        //PARA DEPURAR SOLO, muestra le medida por consola
        Log.i("MEDIDA","Se envía: "+m.getMedida()+" ug/m^3 " +
                "de tipo "+tipoMedida.get(m.getTipo())+" " +
                "en latitud "+m.getUbicacion().getLatitude()+ " longitud "+m.getUbicacion().getLongitude());
        //


        // En funcion de la medida, se obtienen los parametros para la peticion al servidor REST
        String parametros = obtenerParametrosMedidaAPI(m);

        //Se envia la medida a la API REST con los parametros necesarios
        EnvioMedidas envioDeLaMedida = new EnvioMedidas(actividadQueEjecuta);
        envioDeLaMedida.execute(RECURSO_API_MEDIDAS, parametros);




    } // ()


    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Descompone un valor entero de 16 bits en 2 valores
     * simples de 8 bits.
     * --------------------------------------------------------------
     *  Z --> descomponerEn2Bytes() --> EnteroEn2Bytes
     * --------------------------------------------------------------
     * @param valor valor de 16 bits a descomponer en valores de 8 bits
     */
    private EnteroEn2Bytes descomponerEn2bytes(int valor){

        // Mascara de conversion
        int factorConversion = 0b0000000011111111; //0x00FF

        int MSB = valor >> 8; // Desplazamiento de 8 Bits a la derecha para usar como valor solo el MSB
        int LSB = (valor & factorConversion); // Multiplica a nivel de bits por la mascara de conversion para despreciar los primeros 8 bits

        return new EnteroEn2Bytes(MSB, LSB);
    }



    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     *  getTiposMedida() --> Clave/Valor<N, Texto>
     * --------------------------------------------------------------
     * @return HaspMap con los tipos de medidas
     */
    public HashMap<Integer, String> getTiposMedida() {
        return tipoMedida;
    }



    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     *  getUltimoTipoVisto() --> N
     * --------------------------------------------------------------
     * @return Tipo de la ultima medida tomada
     */
    public int getUltimoTipoVisto() {
        return ultimoTipoVisto;
    }



    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     *  getUltimaUbicacionVista() --> Location
     * --------------------------------------------------------------
     * @return Ultima ubicacion conocida
     */
    public Location getUltimaUbicacionVista() {
        return ultimaUbicacionVista;
    }



    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Funcion utilizada para saber si la ubicacion se ha actualizado desde
     * la ultima vez que se solicitio conocer la ubicacion del dispositivo
     * --------------------------------------------------------------
     *  isUbicacionActualizada() --> Booleano
     * --------------------------------------------------------------
     * @return retorna si la ubicacion ha sido actualizada
     */
    public boolean isUbicacionActualizada() {
        return ubicacionActualizada;
    }




    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Funcion para obtener de una Medida un String con el formato requerido
     * con los valores para realizar una peticion al servidor REST
     * --------------------------------------------------------------
     *  Medida --> obtenerParametrosMedidaAPI() --> String
     * --------------------------------------------------------------
     * @param m Medida con un valor, tipo y ubicacion
     * @return retorna un Texto con los valores de la Medida formateados para que la API pueda interpretarlos
     */
    private String obtenerParametrosMedidaAPI(Medida m){

        //Se añaden los parametros del request a la API REST como clave Valor
        HashMap<String, Object> parametros = new HashMap<>();
        parametros.put("valor", m.getMedida());
        parametros.put("tipoMedida", m.getTipo());
        parametros.put("latitud", m.getUbicacion().getLatitude());
        parametros.put("longitud", m.getUbicacion().getLongitude());


        // Se recorre el objeto clave valor y se insertan los valores en un Texto
        Iterator it = parametros.entrySet().iterator();
        boolean primero = true;
        String parametrosString = "";
        while (it.hasNext()){
            if(primero){
                primero = false;
            } else{
                parametrosString +="&"; //Se separan los datos con el caracter '&'
            }

            Map.Entry pair = (Map.Entry)it.next();
            parametrosString +=pair.getKey()+"="+pair.getValue(); //A cada dato se le asigna el valor tras el caracter '='
        }

        //Se devuelven los datos con sus valores como Texto
        return parametrosString;

    }




}
