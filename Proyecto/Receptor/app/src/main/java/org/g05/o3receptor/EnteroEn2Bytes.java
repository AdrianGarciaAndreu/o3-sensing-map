package org.g05.o3receptor;

/**
 * POJO usado para almacenar 2 bytes (MSB y LSB) y acceder a ellos
 * tras haberlos extraido de un valor de 16 bits
 *
 * @author Adrián García Andreu
 * @version 1.1
 */
public class EnteroEn2Bytes {

    // Valores a almacenar
    private int MSB, LSB;

    /**
     * --------------------------------------------------------------
     * Constructor de clase, crea un objeto de la clase
     * con un MSB y un LSB
     * --------------------------------------------------------------
     * Z,Z --> EnteroEn2Bytes()
     * --------------------------------------------------------------
     * @param MSB Byte mas significativo
     * @param LSB Byte menos significativo
     */
    public EnteroEn2Bytes(int MSB, int LSB){
        this.MSB = MSB;
        this.LSB = LSB;
    }


    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * getMSB() --> Z
     * --------------------------------------------------------------
     * @return valor del MSB
     */
    public int getMSB() {
        return MSB;
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * getLSB() --> Z
     * --------------------------------------------------------------
     * @return valor del LSB
     */
    public int getLSB() {
        return LSB;
    }
}
