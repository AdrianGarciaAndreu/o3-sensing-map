<?php

/**
 * Conexion con la BBDD del servidor rest
 * @author Adrian Garcia Andreu
 * @version 1.1
 */


//Nombre del servidor, bbdd, usuario y clave para realizar la conexion a la bbd
$bbddServer = "localhost"; //TODO: Cambiar por servidor publico
$bbddName = "PROYECTO3A";
$bbddUser = "root"; $bbddPass = ""; //TODO: Usar un usuario que no sea el root para las conexiones generales

//Se realiza la conexion y se establece el charset de los datos en utf-8
$conexion = mysqli_connect($bbddServer,$bbddUser,$bbddPass,$bbddName);
mysqli_query($conexion, "SET NAMES utf8");



?>